from tkinter import *
from tkinter import colorchooser
from tkinter.ttk import Style

global default_triangle_color, default_rectangle_color, default_circle_color
global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size

default_triangle_color = "yellow"
default_rectangle_color = "blue"
default_circle_color = "red"
default_triangle_text_color = "black"
default_rectangle_text_color = "black"
default_circle_text_color = "black"
default_triangle_font_size = 14
default_rectangle_font_size = 14
default_circle_font_size = 14


def triangle():
    global default_triangle_color
    global default_triangle_text_color
    global default_triangle_font_size
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_polygon(50, 200, 340, 200, 110, 60,
                                          fill=default_triangle_color, outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Look, it is a triangle')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', default_triangle_font_size), foreground=default_triangle_text_color)


def rectangle():
    global default_rectangle_color
    global default_rectangle_text_color
    global default_rectangle_font_size
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_rectangle(80, 50, 320, 200,
                                            fill=default_rectangle_color, outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Look, it is a rectangle')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', default_rectangle_font_size), foreground=default_rectangle_text_color)


def circle():
    global default_circle_color
    global default_circle_text_color
    global default_circle_font_size
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_oval(50, 50, 300, 300,
                                       fill=default_circle_color, outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Look, it is a circle')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', default_circle_font_size), foreground=default_circle_text_color)


def clear():
    global default_triangle_color, default_rectangle_color, default_circle_color
    global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
    global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size
    global current_shape
    canvas.delete(ALL)
    text.delete('1.0', END)
    current_shape = None


def open_color_window():
    global default_triangle_color, default_rectangle_color, default_circle_color
    global current_shape

    newWindow = Toplevel(root)
    newWindow.title("Modify shape color")
    newWindow.geometry("400x400")
    Label(newWindow, text="This is a space to choose new colors for your shapes").pack()

    def on_choose():
        global default_triangle_color, default_rectangle_color, default_circle_color
        global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
        global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size
        (rgb, hx) = colorchooser.askcolor()
        color_input.delete(0, END)
        color_input.insert(0, hx)

    btn = Button(newWindow, text="Choose Color", command=on_choose)
    btn.place(x=30, y=30)
    color_input = Entry(newWindow, border=1)
    color_input.place(x=160, y=30)

    v = StringVar(newWindow, "1")
    style = Style(newWindow)
    style.configure("TRadiobutton", font=("arial", 10, "bold"))
    values = {"Triangle": 100,
              "Rectangle": 120,
              "Circle": 140,
              }

    entries = {"color": color_input, "shape": v}

    for (key, value) in values.items():
        Radiobutton(newWindow, text=key, variable=v, value=value).place(x=30, y=value)

    b_submit = Button(newWindow, text="Apply", width=15, command=lambda: submit(entries))
    b_submit.place(x=30, y=200)

    def submit(entr):
        global default_triangle_color, default_rectangle_color, default_circle_color
        if int(entr["shape"].get()) == 100:
            default_triangle_color = entr["color"].get()
        elif int(entr["shape"].get()) == 120:
            default_rectangle_color = entr["color"].get()
        elif int(entr["shape"].get()) == 140:
            default_circle_color = entr["color"].get()
        else:
            pass


def open_text_window():
    global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
    global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size
    global current_shape

    newWindow = Toplevel(root)
    newWindow.title("Modify text outlook")
    newWindow.geometry("400x400")
    Label(newWindow, text="This is a space to set up new parameters for a remark").pack()

    def on_choose():
        global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
        global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size
        (rgb, hx) = colorchooser.askcolor()
        color_input.delete(0, END)
        color_input.insert(0, hx)

    btn = Button(newWindow, text="Choose Color", command=on_choose)
    btn.place(x=30, y=30)
    color_input = Entry(newWindow, border=1)
    color_input.place(x=160, y=30)

    v = StringVar(newWindow, "1")
    style = Style(newWindow)
    style.configure("TRadiobutton", font=("arial", 10, "bold"))
    values = {"Triangle": 100,
              "Rectangle": 120,
              "Circle": 140,
              }

    for (key, value) in values.items():
        Radiobutton(newWindow, text=key, variable=v, value=value).place(x=30, y=value)

    label = Label(newWindow, width=22, text="Font size")
    size = Entry(newWindow, border=1)
    label.place(x=10, y=180)
    size.place(x=180, y=180)

    entries = {"color": color_input, "shape": v, "size": size}

    b_submit = Button(newWindow, text="Apply", width=15, command=lambda: submit(entries))
    b_submit.place(x=30, y=220)

    def submit(entr):
        global default_triangle_text_color, default_rectangle_text_color, default_circle_text_color
        global default_triangle_font_size, default_rectangle_font_size, default_circle_font_size
        if int(entr["shape"].get()) == 100:
            default_triangle_text_color = entr["color"].get()
            default_triangle_font_size = entr["size"].get()
        elif int(entr["shape"].get()) == 120:
            default_rectangle_text_color = entr["color"].get()
            default_rectangle_font_size = entr["size"].get()
        elif int(entr["shape"].get()) == 140:
            default_circle_text_color = entr["color"].get()
            default_circle_font_size = entr["size"].get()
        else:
            pass


def file_drop_down_handler(action):
    # Opening a File
    if action == "change_color":
        open_color_window()

    elif action == "change_text":
        open_text_window()


root = Tk()

menu = Menu(root)
fileDropdown = Menu(menu, tearoff=False)
fileDropdown.add_command(label='Images settings', command=lambda: file_drop_down_handler("change_color"))
fileDropdown.add_separator()
fileDropdown.add_command(label='Text settings', command=lambda: file_drop_down_handler("change_text"))
menu.add_cascade(label='Settings menu', menu=fileDropdown)
root.config(menu=menu)

b_triangle = Button(text="Triangle", width=15, command=triangle)
b_rectangle = Button(text="Rectangle", width=15, command=rectangle)
b_circle = Button(text="Circle", width=15, command=circle)
b_clear = Button(text="Clean up", width=15, command=clear)

canvas = Canvas(width=400, height=300, bg='#fff')
text = Text(width=55, height=5, bg='#fff', wrap=WORD)
current_shape = None

b_triangle.grid(row=1, column=0)
b_rectangle.grid(row=2, column=0)
b_circle.grid(row=3, column=0)
b_clear.grid(row=4, column=0)

canvas.grid(row=0, column=1, rowspan=10)
text.grid(row=11, column=1, rowspan=3)

root.mainloop()
